# ToDo

- [x] Upgrade package manager
- [x] Base Package Manager packges
- [x] Flatpak enablement
- [x] Flatpak packages
- [x] Neovim Config
- [x] OhMyZSH Installation
- [x] Starship Installation
- [x] Starship Config
- [x] Hack Nerd Font Setup
- [x] pynvim Installation
- [x] NVM Installation
- [x] SSH Generation
- [x] Enable RPM Fusion
- [x] Link Git Files
- [x] Link Aliases
- [ ] Properly Install Node (nvm install node)
